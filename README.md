To install "x2one mail", download the x2one folder 

Open the Franz Plugins folder on your machine:

    Mac: ~/Library/Application\ Support/Franz/Plugins/
    Windows: %appdata%/Franz/Plugins
    Linux: ~/.config/Franz/Plugins

Alternatively: Go to your Franz settings page, scroll down to the bottom and you will see an option to "Open the Franz plugin directory"

Copy the x2one folder into the plugins directory
    
Restart Franz
